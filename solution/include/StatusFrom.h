

#ifndef STATUSFROM_H
#define STATUSFROM_H
#include "BmpFile.h"
#include <stdio.h>


enum read_status  {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_ERROR
};

enum read_status from_bmp( FILE* in, struct image* img );

#endif
