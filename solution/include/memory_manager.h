#ifndef MEMORY_MANAGER_H
#define MEMORY_MANAGER_H

#include "BmpFile.h"
#include <stddef.h>


void* allocate_memory(size_t size);
void* allocate_array(size_t count, size_t size);
void deallocate_memory(void* ptr);
void deallocate_array(void* ptr);

#endif
