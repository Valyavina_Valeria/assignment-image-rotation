#include "../include/BmpFile.h"
#include "../include/memory_manager.h"

struct image * rotate(struct image  source) {
    struct image *rotatedImage = (struct image *)allocate_memory(sizeof(struct image));
    if (rotatedImage == NULL) {
        return NULL;
    }

    rotatedImage->width = source.height;
    rotatedImage->height = source.width;
    rotatedImage->data = (struct pixel *)allocate_memory(rotatedImage->width * rotatedImage->height * sizeof(struct pixel));

    if (rotatedImage->data == NULL) {
        deallocate_memory(rotatedImage);
        return NULL;
    }

    for (size_t i = 0; i < source.height; ++i) {
        for (size_t j = 0; j < source.width; ++j) {
            if ((rotatedImage->width - j - 1) * rotatedImage->height + i < rotatedImage->width * rotatedImage->height) {
                rotatedImage->data[(rotatedImage->width - j - 1) * rotatedImage->height + i] = source.data[i * source.width + j];
            }
        }
    }

    return rotatedImage;
}
