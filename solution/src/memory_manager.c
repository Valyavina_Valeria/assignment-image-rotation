#include <stdlib.h>

void* allocate_memory(size_t size) {
    return malloc(size);
}

void* allocate_array(size_t count, size_t size) {
    return calloc(count, size);
}

void deallocate_memory(void* ptr) {
    free(ptr);
}

void deallocate_array(void* ptr) {
    free(ptr);
}
