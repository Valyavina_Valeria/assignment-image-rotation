#include "../include/BmpFile.h"
#include "../include/Rotate.h"
#include "../include/StatusFrom.h"
#include "../include/StatusTo.h"
#include "../include/memory_manager.h"

#include <stddef.h>
#include <stdio.h>

int main(int argc, char *argv[]) {
    if (argc == 3) {
     FILE* file = (FILE*)fopen(argv[1], "rb");
     FILE* outFile = (FILE*)fopen(argv[2], "wb");

        if (file == NULL || outFile == NULL) {
            printf("Ошибка открытия файла\n");
            return 1;
        }

        struct image *img = (struct image *)allocate_memory(sizeof(struct image));
        if (img == NULL) {
            printf("Ошибка выделения памяти для изображения\n");
            fclose(file);
            return 1;
        }

        enum read_status status = from_bmp(file, img);

        if (status != READ_OK) {
            printf("Ошибка при чтении изображения\n");
            fclose(file);
            deallocate_memory(img);
            return 1;
        }

        fclose(file);

        const struct image *rotatedImage = rotate(*img);

        if (rotatedImage == NULL || rotatedImage->data == NULL) {
            printf("Ошибка при повороте изображения\n");
            deallocate_memory(img);
            return 1;
        }

        enum write_status write_status = to_bmp(outFile, rotatedImage);
        deallocate_memory(img->data);
        deallocate_memory(img);
        fclose(outFile);

        if (write_status != WRITE_OK) {
            printf("Ошибка при записи изображения\n");
            return 1;
        }
    }
    return 0;
}
