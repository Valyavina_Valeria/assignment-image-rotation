#include "../include/BmpFile.h"
#include "../include/StatusTo.h"

#include <stddef.h>
#include <stdio.h>

enum write_status to_bmp(FILE *out, const struct image *img) {
    if (out == NULL || img == NULL) {
        return WRITE_ERROR;
    }

    struct bmp_header header;
    header.biWidth = img->width;
    header.biHeight = img->height;

    fwrite(&header, sizeof(struct bmp_header), 1, out);

    uint32_t padding = (4 - (img->width * sizeof(struct pixel)) % 4) % 4;


    for (uint32_t i = 0; i < img->height; i++) {
        fwrite(&(img->data[i * img->width]), sizeof(struct pixel), img->width, out);

        for (int j = 0; j < padding; j++) {
            uint8_t paddingByte = 0;
            fwrite(&paddingByte, sizeof(uint8_t), 1, out);
        }
    }

    return WRITE_OK;
}
