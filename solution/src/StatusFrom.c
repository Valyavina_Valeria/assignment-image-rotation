#include "../include/BmpFile.h"
#include "../include/StatusFrom.h"
#include "../include/memory_manager.h"

#include <stddef.h>
#include <stdio.h>

enum read_status from_bmp(FILE *in, struct image *img) {
    if (in == NULL) {
        return READ_INVALID_BITS;
    }

    struct bmp_header header;
    size_t headerRead = fread(&header, sizeof(struct bmp_header), 1, in);

    if (headerRead != 1 ) {
        return READ_INVALID_SIGNATURE;
    }

    if (header.biWidth <= 0 || header.biHeight <= 0) {
        return READ_INVALID_HEADER;
    }

    uint64_t width = header.biWidth;
    uint64_t height = header.biHeight;
    uint64_t rowSize = width * sizeof(struct pixel);

    uint64_t padding = (4 - rowSize % 4) % 4;

    struct pixel *imageData = (struct pixel *)allocate_memory(height * rowSize);
    if (imageData == NULL) {
        return READ_ERROR;
    }

    for (uint64_t i = 0; i < height; ++i) {
        size_t pixelsRead = fread(&imageData[i * width], sizeof(struct pixel), width, in);
        if (pixelsRead != width) {
            deallocate_memory(imageData);
            return READ_INVALID_BITS;
        }
        fseek(in, (long)padding, SEEK_CUR);
    }

    img->data = imageData;
    img->header = header;
    img->width = width;
    img->height = height;
    return READ_OK;
}
